import * as plugins from './smartexpress.plugins.js';
import { Handler } from './smartexpress.classes.handler.js';

import * as interfaces from './interfaces/index.js';

export class HandlerTypedRouter extends Handler {
  /**
   * The constuctor of HandlerProxy
   * @param remoteMountPointArg
   */
  constructor(typedrouter: plugins.typedrequest.TypedRouter) {
    super('POST', async (req, res) => {
      const response = await typedrouter.routeAndAddResponse(req.body);
      res.json(response);
    });
  }
}
