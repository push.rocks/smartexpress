// node native
import * as http from 'http';
import * as https from 'https';
import * as net from 'net';
import * as path from 'path';

export { http, https, net, path };

// apiglobal
import * as typedrequest from '@apiglobal/typedrequest';
import * as typedsocket from '@apiglobal/typedsocket';

export { typedrequest, typedsocket };

// pushrocks scope
import * as lik from '@pushrocks/lik';
import * as smartdelay from '@pushrocks/smartdelay';
import * as smartenv from '@pushrocks/smartenv';
import * as smartfeed from '@pushrocks/smartfeed';
import * as smartfile from '@pushrocks/smartfile';
import * as smartmanifest from '@pushrocks/smartmanifest';
import * as smartmime from '@pushrocks/smartmime';
import * as smartpath from '@pushrocks/smartpath';
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartrequest from '@pushrocks/smartrequest';
import * as smartsitemap from '@pushrocks/smartsitemap';
import * as smarttime from '@pushrocks/smarttime';

export {
  lik,
  smartdelay,
  smartenv,
  smartfeed,
  smartfile,
  smartmanifest,
  smartmime,
  smartpath,
  smartpromise,
  smartrequest,
  smartsitemap,
  smarttime,
};

// @tsclass scope
import * as tsclass from '@tsclass/tsclass';

export { tsclass };

// express
import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
// @ts-ignore
import expressForceSsl from 'express-force-ssl';

export { bodyParser, cors, express, expressForceSsl };
