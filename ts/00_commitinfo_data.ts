/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartexpress',
  version: '4.0.35',
  description: 'express apis the smart way'
}
